from distutils.core import setup, Extension


def main():
    setup(
        name="summer",
        version="1.0.0",
        description="Calculate sum of n numbers.",
        author="gonczor",
        author_email="",
        ext_modules=[Extension("summer", ["summermodule.c"], extra_compile_args = ["-O0", "-pthread"])],
    )


if __name__ == "__main__":
    main()
