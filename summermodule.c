#include <Python.h>
#include <pthread.h>


void* sum(void *argp){
    unsigned long *stepsFrom = (unsigned long *) argp;
    unsigned long *stepsTo = (unsigned long *) (argp + sizeof(unsigned long));
    unsigned long s, i;

    s = 0;
    for(i=*stepsFrom; i<*stepsTo; i++)
        s += (i+1);

    return (void *)s;
}


static PyObject* method_sum(PyObject* self, PyObject* args) {
    unsigned long countOfNumbers;
    if(!PyArg_ParseTuple(args, "k", &countOfNumbers)){
        printf("# FAIL\n");
        return NULL;
    }

    // Split summing into threads
    unsigned long firstStepsHalf[2] = {0, countOfNumbers/2};
    unsigned long secondStepsHalf[2] = {countOfNumbers/2, countOfNumbers};
    void *result1, *result2;

    pthread_t thread1, thread2;
    pthread_create(&thread1, NULL, sum, firstStepsHalf);
    pthread_create(&thread2, NULL, sum, secondStepsHalf);
    pthread_join(thread1, &result1);
    pthread_join(thread2, &result2);
    return PyLong_FromLong((unsigned long)result1+(unsigned long)result2);
}

static PyMethodDef SumMethods[] = {
    {"sum", method_sum, METH_VARARGS, "Calculate sum of n numbers."},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef summodule = {
    PyModuleDef_HEAD_INIT,
    "sum",
    "Calculate sum of n numbers.",
    -1,
    SumMethods
};

PyMODINIT_FUNC PyInit_summer() {
    return PyModule_Create(&summodule);
}